package com.vgdragon.folderTransver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.io.File

@SpringBootApplication
class Hello


fun main(args: Array<String>) {

    if(args.isNotEmpty()){
        val file = File(args[0])
        if(file.isDirectory){
            workFolder = file
        }
    } else {
        workFolder = File("")
    }

    runApplication<Hello>(*args)
}


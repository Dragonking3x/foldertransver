package com.vgdragon.folderTransver




import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.io.File
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.zip.ZipOutputStream
import java.util.zip.ZipEntry
import java.io.FileInputStream


@Controller
class IndexController () {

    private val simpleDateFormat = SimpleDateFormat("yyyy.MM.dd - HH.mm")




    @GetMapping("/")
    @ResponseBody
    fun indexControllerGet(model: Model): String {
        var returnHtml = "<!DOCTYPE html>" +
                "<head>\n" +
                "    <title>Downloads</title>\n" +
                "\n" +
                "    <meta name=\"description\" content=\"Downloads\"/>\n" +
                "\n" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" +
                "\n" +
                "\n" +
                "</head>" +
                "<body>\n" +
                "\n"

        val listFiles = workFolder.listFiles()
        if(listFiles.isNotEmpty()) {
            for (file in listFiles) {
                if(!file.isDirectory)
                    continue
                returnHtml += "<a href=\"\\download?fileName=${file.name}\"> ${file.name} </a>" +
                    "</br>"
            }
        }



        return returnHtml +  "</body>"
    }

    @GetMapping("/download")
    fun downloadControllerGet(@RequestParam(value = "fileName", defaultValue = "") fileName: String, model: Model): StreamingResponseBody {
        val file = File("${workFolder.absolutePath}\\$fileName")

        return folderToZip(file)
    }


    fun folderToZip(file: File): StreamingResponseBody {
        return StreamingResponseBody { outputStream ->

            zipping(file, outputStream)
        }
    }


    fun zipping(fileToZip: File, outputStream: OutputStream) {
        val zipOut = ZipOutputStream(outputStream)
        zipFile(fileToZip, fileToZip.name, zipOut)
        zipOut.close()
        outputStream.close()
    }


    private fun zipFile(fileToZip: File, fileName: String, zipOut: ZipOutputStream) {
        if (fileToZip.isHidden) {
            return
        }
        if (fileToZip.isDirectory) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(ZipEntry(fileName))
                zipOut.closeEntry()
            } else {
                zipOut.putNextEntry(ZipEntry("$fileName/"))
                zipOut.closeEntry()
            }
            val children = fileToZip.listFiles()
            for (childFile in children!!) {
                zipFile(childFile, fileName + "/" + childFile.name, zipOut)
            }
            return
        }
        val fis = FileInputStream(fileToZip)
        val zipEntry = ZipEntry(fileName)
        zipOut.putNextEntry(zipEntry)
        val bytes = ByteArray(1024)
        var length: Int = fis.read(bytes)


        while (length  >= 0) {
            zipOut.write(bytes, 0, length)
            length = fis.read(bytes)
        }
        fis.close()
    }

}